function type_fit = integerize(A)
% 1. Write a function called integerize that takes as its input a matrix A
% of non-negative integers of type double, and returns the name of the
% “smallest” unsigned integer class to which A can be accurately converted.
% If no such class exists, the string 'NONE' is returned. For example, if
% the largest element of A is 14, then the function would return 'uint8',
% but if the largest integer in A is 1e20, then the function would return
% 'NONE'.

maxval = max(max(A));
if maxval < 256
    type_fit = 'uint8';
elseif maxval < 65536
    type_fit = 'uint16';
elseif maxval < 4294967296
    type_fit = 'uint32';
elseif maxval < 18446744073709551616
    type_fit = 'uint64';
else
    type_fit = 'NONE';
end
