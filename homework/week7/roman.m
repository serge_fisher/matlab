function integer = roman(number)

romans = {'I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX', 'X',...
         'XI','XII','XIII','XIV','XV','XVI','XVII','XVIII','XIX','XX'};
     
for i = 1:20
    if strcmp(number, romans{i})
        integer = uint8(i);
        return;
    end
end

integer = uint8(0);
