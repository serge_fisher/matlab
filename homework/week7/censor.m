function rv = censor(cv, ban)

j = 1;
rv = cell(0);
for i = 1:length(cv)
    a = findstr(cv{i}, ban);
    if isempty(a)
        rv{j} = cv{i};
        j = j +1;
    end
end
