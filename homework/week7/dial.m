function validated = dial(str)

validated = char(zeros(1, length(str)) +32);

for i = 1:length(str)
    if str(i) == '#' || str(i) == '*' || str(i) == ' '
        validated(i) = str(i);
    elseif str(i) == '(' || str(i) == ')' || str(i) == '-'
        validated(i) = ' ';
    elseif str(i) < 58 && str(i) > 47
        validated(i) = str(i);
    elseif str(i) < 'Q' && str(i) > 64
        validated(i) = char( fix((str(i) - 65)/3) +50);
        fprintf('e:%s', validated(i));
    elseif str(i) > 'Q' && str(i) < 90
        validated(i) = char( fix((str(i) - 81)/3) +55);
    else
        validated = [];
        return;
    end
end
