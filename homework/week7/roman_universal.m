function integer = roman(number)

integer = 0;
prev = 0;
for i = length(number):-1:1
    if number(i) == 'I'
        cur = 1;
    elseif number(i) == 'V'
        cur = 5;
    elseif number(i) == 'X'
        cur = 10;
    else
        integer = 0;
        return;
    end
    if cur >= prev
        integer = integer +cur;
        prev = cur;
    else
        integer = integer -cur;
        prev = cur;
    end
end

integer = uint8(integer);
