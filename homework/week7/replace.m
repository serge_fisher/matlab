function cells = replace(vec, c1, c2)
% 6. Write a function called replace that takes three inputs: a cell vector
% (row or column) of strings and two characters: c1 and c2. The function
% returns the cell vector unchanged except that each instance of c1 in each
% string is replaced by c2. You are not allowed to use the built-in
% function strrep.

for i = 1:length(vec)
    %%cells(i) = strrep(vec{i}, c1, c2);
    cells(i) = vec(i);
    pos = strfind(vec{i}, c1);
    for p = pos
        cells{i}(p) = c2;
    end
end
