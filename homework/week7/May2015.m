function m = May2015()
% 2. Write a function called May2015 that returns a struct vector (row or
% column) whose elements correspond to the days of May, 2015. Each struct
% should contain three fields with these (exact) field names: “month”,
% “date”, and “day” (all lower case).

weekdays = [{'Fri'}, {'Sat'}, {'Sun'}, {'Mon'}, {'Tue'}, {'Wed'}, {'Thu'}];

wd = 1;
for d = 1:31
    m(d).month = 'May';
    m(d).date = d;
    m(d).day = weekdays{wd};
    wd = wd +1;
    if wd == 8
        wd = 1;
    end
end

    