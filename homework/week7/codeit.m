function encoded = codeit(txt)
% 4. Write a function called codeit that takes a string txt as input and
% returns another string coded as output. The function reverses the
% alphabet: It replaces each 'a' with 'z', each 'b' with 'y', each 'c' with
% 'x', etc. The function must work for uppercase letters the same way, but
% it must not change any other characters. Note that if you call the
% function twice like this str = codeit(codeit(txt)) then str and txt will
% be identical.

codetable = 26:-1:1;
for i = 1:length(txt)
    sym = double(txt(i));
    if sym > 64 && sym < 91
        enc = codetable(sym -64) +64;
        encoded(i) = char(enc);
    elseif sym > 96 && sym < 123
        enc = codetable(sym -96) +96;
        encoded(i) = char(enc);
    else
        encoded(i) = txt(i);
    end
end
