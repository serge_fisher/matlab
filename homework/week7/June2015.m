function cell_array = June2015

wd = [{'Mon'}, {'Tue'}, {'Wed'}, {'Thu'}, {'Fri'}, {'Sat'}, {'Sun'}];
cell_array = cell(30,3);

wdi = 1;
for i = 1:30
    cell_array{i, 1} = 'June';
    cell_array{i, 2} = i;
    cell_array{i, 3} = wd{wdi};
    wdi = wdi +1;
    if wdi == 8; wdi = 1; end
end
