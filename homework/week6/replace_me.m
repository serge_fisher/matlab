function w = replace_me(v, a, b, c)
% 2. The function replace_me is defined like this: function w =
% replace_me(v,a,b,c). The first input argument v is a vector, while a, b,
% and c are all scalars. The function replaces every element of v that is
% equal to a with b and c. For example, the command >> x = replace_me([1 2
% 3],2,4,5); makes x equal to [1 4 5 3]. If c is omitted, it replaces
% occurrences of a with two copies of b. If b is also omitted, it replaces
% each a with two zeros.

if ~isvector(v)
    error ('first argument should be a vector of integers')
end

if nargin < 2
    error ('must have at least two arguments, v and a')
end

if nargin < 3
    b = 0;
end

if nargin < 4
    c = b;
end

j = 1;
for i = 1:length(v)
    if v(i) == a
        w(j) = b;
        w(j+1) = c;
        j = j +2;
    else
        w(j) = v(i);
        j = j +1;
    end
end
