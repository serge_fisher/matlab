function [ap, k] = approximate_pi(delta)
% 6. Write a function called approximate_pi that uses the following
% approximation of π:
% 
%      _  ∞    (−3) −k
% π = √12 ∑   ---------
%        k=0    2k + 1
% 
% Instead of going to infinity, the function stops at the smallest k for
% which the approximation differs from pi (i.e., the value returned
% MATLAB’s built-in function) by no more than the positive scalar, delta,
% which is the only input argument. The first output of the function is the
% approximate value of π, while the second is k. (Note: if your program or
% the grader takes a long time, you may have created an infinite loop and
% need to hit Ctrl-C on your keyboard.)


sq12 = sqrt(12);
k = -1;
ap = 0;
df = sq12*ap - pi;
while abs( df ) >= delta
    k = k +1;
    inc = ((-1/3)^k)/(2*k +1);
    ap = ap + inc;
    df = pi - sq12*ap;
end

ap = ap * sq12;
