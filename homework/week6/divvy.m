function B = divvy(A, k)
% 8. Write a function called divvy that takes a matrix A of positive
% integers and a single positive integer k as its two inputs and returns a
% matrix B that has the same size as A. The elements of B are all divisible
% by k. If an element of A is divisible by k, then the corresponding
% element in B must have the same value. If an element of A is not
% divisible by k, then the corresponding element of B must be the product
% of the given element of A and k. You are not allowed to use any for-loops
% or while-loops. For example, the call X = divvy([1 2 ; 3 4], 2) would
% make X equal to [2 2 ; 6 4].

[rows, cols] = size(A);
B = zeros(rows, cols);

B(rem(A, k)==0) = A(rem(A, k) == 0);
B(rem(A, k)~=0) = A(rem(A, k) ~= 0) *k;
