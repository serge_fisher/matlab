function index = large_elements(X)
% 4. Write a function called large_elements that takes as input an array
% named X that is a matrix or a vector. The function identifies those
% elements of X that are greater than the sum of their two indexes. For
% example, if the element X(2,3) is 6, then that element would be
% identified because 6 is greater than 2 + 3. The output of the function
% gives the indexes of such elements found in row- major order. It is a
% matrix with exactly two columns. The first column contains the row
% indexes, while the second column contains the corresponding column
% indexes. For example, the statement indexes = large_elements([1 4; 5 2; 6
% 0], will make indexes equal to [1 2; 2 1; 3 1]. If no such element
% exists, the function returns an empty array.

index = [];
[rows, cols] = size(X);

for i= 1:rows
    for j = 1:cols
        if X(i,j) > i+j
            index = [index; i j];
        end
    end
end
