function sumv = square_wave(n)
% 9. *** Write a function called square_wave that computes the sum
% 
%  n    sin((2k − 1)t)
%  ∑   ----------------
% k=1     (2k − 1)
% 
% for each of 1001 values of t uniformly spaced from 0 to 4π inclusive. The
% input argument is a positive scalar integer n, and the output argument is
% a row vector of 1001 such sums—one sum for each value of t. You can test
% your function by calling it with n == 200 or greater and plotting the
% result, and you will see why the function is called “square_wave”.

sumv = zeros(1,1001);
i = 1;
for t = 0:((4*pi)/1000):(4*pi)
    sum = 0;
    for k = 1:n
        inc = sin((2*k -1) *t) / (2*k -1);
        sum = sum + inc;
    end
    sumv(i) = sum;
    i = i +1;
end
