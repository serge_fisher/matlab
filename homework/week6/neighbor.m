function w = neighbor (v)
% 1. Write a function called neighbor that takes as input a row vector
% called v and creates another row vector as output that contains the
% absolute values of the differences between neighboring elements of v. For
% example, if v == [1 2 4 7], then the output of the function would be [1 2
% 3]. Notice that the length of the output vector is one less than that of
% the input. Check that the input v is indeed a vector and has at least two
% elements and return an empty array otherwise. You are not allowed to use
% the diff built-in function.
[r, ~] = size(v);
if (~isvector(v) || length(v) <2 || r ~= 1)
    w = [];
    return;
end

for n = 2:length(v)
    w(n-1) = abs(v(n-1) - v(n));
end
