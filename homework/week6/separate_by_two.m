function [even, odd] = separate_by_two(A)
% 7. Write a function called separate_by_two that takes a matrix A of
% positive integers as an input and returns two row vectors. The first one
% contains all the even elements of A and nothing else, while the second
% contains all the odd elements of A and nothing else, both arranged
% according to column-major order of A. You are not allowed to use
% for-loops or while-loops.

odd = A(rem(A, 2) ~= 0)';
even = A(rem(A, 2)== 0)';

