function bool = myprime(n)
% 10. *** Write a function myprime that takes n, a positive integer, as an
% input and returns true if n is prime or returns false otherwise. Do not
% use the isprime or primes or factor built-in functions. Hint: you can use
% the rem or fix functions.

for i = fix(sqrt(n)):-1:2
    if rem(n, i) == 0
        bool = false;
        return
    end
end
bool = true;
