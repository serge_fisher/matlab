function s = halfsum (A)
% 3. Write a function called halfsum that takes as input an at most
% two-dimensional matrix A and computes the sum of the elements of A that
% are in the diagonal or are to the right of it. The diagonal is defined as
% the set of those elements whose column and row indexes are the same. For
% example, if the input is [1 2 3; 4 5 6; 7 8 9], then the function would
% return 26.

[rows cols] = size(A);

m = min(rows, cols);

s = 0;
for i = 1:m
    for j = i:cols
        s = s + A(i,j);
    end
end
