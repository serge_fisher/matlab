function n = one_per_n(x)
% 5. Write a function called one_per_n that returns the smallest positive
% integer n for which the sum 1 + 1/2 + 1/3 + ... + 1/n, is greater than or
% equal to x where the scalar x is the input argument. Limit the maximum
% number n of terms in the sum to 10,000 and return -1 if it is exceeded.
% (Note: if your program or the grader takes a long time, you may have
% created an infinite loop and need to hit Ctrl-C on your keyboard.)
n = 1;
sum = 0;
for n = 1:10000
    sum = sum + 1/n;
    if sum >= x
        return
    end
end
n = -1;