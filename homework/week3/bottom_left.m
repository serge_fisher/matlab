% 6. Write a function called bottom_left that takes two inputs: a matrix N
% and a scalar n, in that order, where each dimension of N is greater than
% or equal to n. The function returns the n-by-n square array at the bottom
% left corner of N.

function a = bottom_left(N, n)
a = N(1-n+end:end, 1:n);
