% 3. Suppose we have a pile of coins. Write a function called rich that
% computes how much money we have. It takes one input argument that is a
% row vector whose 4 elements specify the number of pennies (1 cent),
% nickels (5 cents), dimes (10 cents), and quarters (25 cents) that we have
% (in the order listed here). The output of the function is the value of
% the total in dollars (not cents). For example, if we had five coins, four
% quarters and a penny, the answer would be 1.01.

function dollars = rich(coins)
values = [0.01, 0.05, 0.1, 0.25]';
dollars = coins * values;
