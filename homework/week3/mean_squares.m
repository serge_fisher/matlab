% 7. Write a function called *mean_squares* that returns *mm*, which is the
% *mean* of the squares of the first *nn* positive integers, where nn is a
% positive integer and is the only input argument. For example, if _nn_ is
% 5, your function needs to compute the average of the numbers 1, 4, 9, 16,
% and 25. You may use any built-in functions including, for example, sum.

function mm = mean_squares(nn)
mm = mean( ( 1:nn).^2 );
