function gen = generationXYZ(year)
% GENERATIONXYZ - gives generation by year
%    Write a function called generationXYZ that takes as its only input
%    argument one positive integer specifying the year of birth of a person
%    and returns as its only output argument the name of the generation
%    that the person is part of ('X', 'Y', or 'Z ') according to the table
%    below. For births _before_ 1966, return 'O' for Old and for births
%    _after_ 2012, return 'K' for Kid. Remember that to assign a letter to
%    a variable, you need to put it in single quotes, as in: gen = 'X'.
% 
% <html>
% <table border=1>
% <tr><th>Generation</th><th>Born</th></tr>
% <tr><td>X</td><td>1966-1980</td></tr>
% <tr><td>Y</td><td>1981-1999</td></tr>
% </table>
% </html>
if year <1966
    gen = 'O'
elseif year <1981
    gen = 'X'
elseif year < 2000
    gen = 'Y'
elseif year < 2013
    gen = 'Z'
else
    gen = 'K'
end


return
