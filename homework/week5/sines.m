function [s1, s2, sums] = sines(pts,amp,n1,n2)

if nargin <1
    pts = 1000;
end
if nargin <2
    amp = 1;
end
if nargin <3
    n1 = 100;
end
if nargin <4
    n2 = n1 *1.05;
end

end1 = n1 * 2 *pi;
end2 = n2 * 2 *pi;

ran1 = (end1)/(pts-1);
ran2 = (end2)/(pts-1);

s1 = sin(0:ran1:end1) *amp;
s2 = sin(0:ran2:end2) *amp;
sums = s1 + s2;
