function v = sort3(x,y,z)
% 3. Write a function called SORT3 that takes three scalar arguments. It
% uses if-statements, possibly nested, to return the three values of these
% arguments in a single row-vector in increasing order (or more precisely,
% non-decreasing order), i.e., element one of the output vector equals the
% smallest input argument and element three of the output vector equals the
% largest input argument. NOTE: Your function may not use any built-in
% functions, e.g., SORT

if x <= y && x <= z
    if y <= z
        v = [x y z];
    else
        v = [x z y];
    end
elseif y <= x && y <= z
    if x <= z
        v = [y x z];
    else
        v = [y z x];
    end
else
    if y <= x
        v = [z y x];
    else
        v = [z x y];
    end
end
