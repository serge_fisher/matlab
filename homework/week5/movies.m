function bool = movies(hr1, min1, durmin1, hr2, min2, durmin2)
% 6. Write a function called movies that takes the starting times of two
% movies, hr1, hr2, min1, min2, and their durations, durmin1, durmin2, as
% its input arguments and decides whether we can binge and watch both. The
% criteria are that they must not overlap and that we are not going to wait
% more than 30 minutes between the end of one and the beginning of the
% next. It returns true if the criteria are both met and returns false
% otherwise. You may assume that movie start times are always after 1 pm
% and before midnight. You may also assume that the first one starts
% earlier. The order of the input arguments is: hr1, min1, durmin1, hr2,
% min2, durmin2.

start1 = hr1*60 + min1;
start2 = hr2*60 + min2;

end1 = start1 + durmin1;
end2 = start2 + durmin2;

if end1 > start2 || (start2 - end1) >30
    bool = false;
else
    bool = true;
end
