function retval = classify(x)
%4. Write a function called CLASSIFY that takes one input argument x. That
%  argument will have no more than two dimensions. If x is an empty matrix,
%  the function returns -1. If x is a scalar, it returns 0. If x is a
%  vector, it returns 1. Finally, if x is none of these, it returns 2. Do
%  not use the built-in functions isempty, isscalar, or isvector.

[rows, cols] = size(x)
if rows == 0 || cols == 0
    retval = -1;
elseif size(x) == [1 1]
    retval = 0;
elseif rows == 1 || cols == 1
    retval = 1;
else
    retval = 2;
end
