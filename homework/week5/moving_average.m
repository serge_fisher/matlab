function s = moving_average(x)
%
% 8. *** Write a function called MOVING_AVERAGE that takes a scalar called
% x as an input argument and returns a scalar. The value it returns depends
% not only on the input but also on previous inputs to this same function
% when the function is called repeatedly. That returned value is a “moving
% average” of those inputs. The function uses a “buffer” to hold previous
% inputs, and the buffer can hold a maximum of 25 inputs. Specifically, the
% function must save the most recent 25 inputs in a vector (the buffer)
% that is a persistent variable inside the function. Each time the function
% is called, it copies the input argument into an element of the buffer. If
% there are already 25 inputs stored in the buffer, it discards the oldest
% element and saves the current one in the buffer. After it has stored the
% input in the buffer, it returns the mean of all the elements in the
% buffer. Thus, for each of the first 24 calls to the function, the
% function uses only the inputs it has received so far to determine the
% average (e.g., the first call simply returns x, the second call averages
% x and the input from the first call, etc.), and after that, it returns
% the average of the most recent 25 inputs.

persistent buffer

if isempty(buffer)
    buffer = x;
    s = x;
    return
end

if length(buffer) >24
    buffer = [buffer(2:end) x];
else
    buffer = [buffer(1:end) x];
    fprintf('new buffer is %d\n', buffer);
end

s = mean (buffer);
