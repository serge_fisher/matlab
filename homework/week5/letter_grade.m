function grade = letter_grade (score)
%2. Write a function called LETTER_GRADE that takes a positive integer
%   called SCORE as its input argument and returns a letter GRADE according
%   to the following scale: A: 91 and above; B: 81-90; C: 71-80; D: 61-70;
%   F: below 61. Remember that to assign a letter to a variable, you need
%   to put it in single quotes, as in: grade = 'A'.

if score > 90
    grade = 'A'
elseif score > 80
    grade = 'B'
elseif score > 70
    grade = 'C'
elseif score > 60
    grade = 'D'
else
    grade = 'F'
end
