function tr = bell(n)

if n <= 0 || fix(n) ~= n
    tr = [];
    return;
end

% if n == 1
%     tr = [1];
%     return;
% end

tr = zeros(n,n);
tr(1,1) = 1;

for i = 2:n
    tr(i, 1) = tr(1,i-1);
    for j = 2:i
        prev1 = tr((i+1) -j, j -1);
        prev2 = tr((i+2) -j, j -1);
        tr((i+1) -j, j) =  prev1 + prev2;
    end
end
