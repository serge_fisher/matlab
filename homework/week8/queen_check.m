function bool = queen_check(board)
% 7. *** Write a function called queen_check that takes an 8-by-8 logical
% array called board as input. The variable board represents a chessboard
% where a true element means that there is a queen at the given position
% (unlike in chess, there can be up to 64 queens!), and false means that
% there is no piece at the given position. The function returns true if no
% two queens threaten each other and false otherwise. One queen threatens
% the other if they are on the same row, on the same column, or on the same
% diagonal.
w=8;
bool = true;
for i = 1:w
    for j = 1:w
        if board(i,j) == 1
            cross = sum(board(i,:)) + sum(board(:,j));
            cross = cross + sum(diag(board, j-i));
            cross = cross + sum(diag(rot90(board), i -w -1 +j));
            if cross ~= 4
                bool = false;
                return;
            end
        end
    end
end
