function point = saddle(M)
% 4. Write a function called saddle that finds saddle points in the input
% matrix M. For the purposes of this problem, a saddle point is defined as
% an element whose value is greater than or equal to every element in its
% row, and less than or equal to every element in its column. Note that
% there may be more than one saddle point in M. Return a matrix called
% indices that has exactly two columns. Each row of indices corresponds to
% one saddle point with the first element of the row containing the row
% index of the saddle point and the second column containing the column
% index. The saddle points are provided in indices in the same order they
% are located in M according to column-major ordering. If there is no
% saddle point in M, then indices is the empty array.
point = [];

[rows, cols] = size(M);
for j = 1:cols
    for i = 1:rows
        if M(i,j) >= max(M(i, :)) && M(i,j) <= min(M(:,j))
            point = [point; i j];
        end
    end
end

            