function dec = roman2(roman)
% 9. *** Write a function called ROMAN2 that takes a string input
% representing an integer between 1 and 399 inclusive using Roman numerals
% and returns the Arabic equivalent as a uint16. If the input is illegal,
% or its value is larger than 399, roman2 returns 0 instead. The rules for
% Roman numerals can be found here:
% http://en.wikipedia.org/wiki/Roman_numerals. Use the definition at the
% beginning of the page under the “Reading Roman Numerals” heading. In
% order to have unambiguous one-to-one mapping from Roman to Arabic
% numbers, consider only the shortest possible roman representation as
% legal. Therefore, only three consecutive symbols can be the same (IIII or
% VIIII are illegal, but IV and IX are fine). Also, a subtractive notation
% cannot be followed by an additive one using the same symbols making
% strange combinations, such as IXI for 10 or IXX for 19, illegal also.

if length(roman) <1
    dec = uint16(0);
    return;
end

%% first pass, just a mapping
prev = 0; counter = 1;
for i = 1:length(roman)
    if roman(i) == 'M' roman(i) = 1000;
    elseif roman(i) == 'D' roman(i) = 500;
    elseif roman(i) == 'C' roman(i) = 100;
    elseif roman(i) == 'L' roman(i) = 50;
    elseif roman(i) == 'X' roman(i) = 10;
    elseif roman(i) == 'V' roman(i) = 5;
    elseif roman(i) == 'I' roman(i) = 1;
    else
        dec = uint16(0);
        return;
    end

    % special treatment of disallowed consecutive 4 same digits
    if roman(i) == prev
        counter = counter +1;
    else
        counter = 1;
        prev = roman(i);
    end
    if counter >3
        dec = uint16(0);
        return;
    end
end

%% second pass, forming the answer
prev = roman(1); dec = roman(1);
for i = 2:length(roman)
    if roman(i-1) < roman(i)
        dec = dec - roman(i-1)*2 + roman(i);
        if i+1 <= length(roman) && roman(i+1) >= roman(i-1)
            dec = uint16(0);
            return;
        end
    else
        dec = dec + roman(i);
    end
    prev = roman(i);
end

if dec > 399
    dec = uint16(0);
else
    dec = uint16(dec);
end

