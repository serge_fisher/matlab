function [row,col,numrows,numcols,summa] = maxsubsum(A)

[rows,cols] = size(A);
summa = sum(sum(A));
row = 1; col = 1; numrows = rows; numcols = cols;
for i = 1:rows
    for j = 1:cols
        for ii = i:rows
            for jj = j:cols
                csum = sum(sum(A(i:ii,j:jj)));
                if csum > summa
                    summa = csum;
                    row = i; col = j;
                    numrows = ii - i +1;
                    numcols = jj - j +1;
                end
            end
        end
    end
end

                    