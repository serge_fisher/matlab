function pts = bowl(result)

pts = 0;
additional = 0;
frame = 1;
bonus = 0;
for i = 1:length(result)
    if result(i) >10 || result(i) <0 || frame >11
        pts = -1;
        return;
    end
    if bonus >0
        bonus = bonus -1;
        pts = pts + result(i);
    elseif additional == 1
        % check if two balls sums more than 10 -- error
        if result(i) + result(i-1) >10
            pts = -1;
            return;
        end
        if result(i) + result(i-1) == 10 % spare
            pts = pts + 10;
            if i+1 <= length(result) && frame ~= 10
                pts = pts + result(i+1);
            end
            fprintf('%d! [%d] |', result(i), pts);
            if frame == 10
                bonus = 1;
            end
        else % open
            pts = pts + result(i-1) + result(i);
            fprintf('%d. [%d] |', result(i), pts);
        end
        frame = frame +1;
        additional = 0;
    else % first bowl in a frame
        fprintf('(%d): ', frame);
        if result(i) <10 % first bowl <10
            additional = 1;
            fprintf('%d, ', result(i));
        else % strike
            pts = pts +10;
            if i+1 <= length(result) && frame ~=10
                pts = pts + result(i+1);
            end
            if i+2 <= length(result) && frame ~= 10
                pts = pts + result(i+2);
            end
            if frame == 10
                bonus = 2;
            end
            frame = frame +1;
            fprintf('%dX [%d] |', result(i), pts);
        end
    end % if first ball in frame
    
end

fprintf('\n');

if frame ~=11 || (frame == 11 && additional == 1) || bonus ~= 0
    fprintf('additional = %d, frame = %d, bonus = %d\n', additional, frame, bonus);
    pts = -1;
    return;
end

