function result = sparse_array_out(A, filename)
% 1. Write a function called SPARSE_ARRAY_OUT that takes two input
% arguments and returns one output argument. Its first argument is a
% two-dimensional array of doubles, which it writes into a binary file. The
% name of that file is the second argument to the function. The file must
% have the following format. It starts with three uint32 scalars specifying
% the number of rows of the array followed by the number of columns
% followed by the number of non-zero elements in the array. Then each
% non-zero element of the array is represented by two uint32 scalars and a
% double scalar in the file in this order: its row index (uint32), its
% column index (uint32), and its value (double). Note that this file format
% is an efficient way to store a so-called “sparse array”, which is by
% definition an array for which the overwhelming majority of its elements
% equal 0. The function’s output argument is of type logical and equals
% false if there was a problem opening the file and true otherwise.
fid = fopen(filename, 'w+');
if (fid < 0)
    result = false;
    return;
end
[rows, cols] = size(A);
nonzero = sum(sum(A ~= 0));
fwrite(fid, rows, 'uint32');
fwrite(fid, cols, 'uint32');
fwrite(fid, nonzero, 'uint32');
for i = 1:rows
    for j = 1:cols
        if A(i,j) ~= 0
            fwrite(fid, i, 'uint32');
            fwrite(fid, j, 'uint32');
            fwrite(fid, A(i,j), 'double');
        end
    end
end
fclose(fid);
result = true;
