function wc = letter_counter(filename)
% 3. Write a function called LETTER_COUNTER that takes the name of a text
% file as input and returns the number of letters (i.e., any of the
% characters, a-to-z and A-to-Z) that the file contains. HINT: You can use
% the built-in function isletter. If there is a problem opening the file,
% the function returns -1. WARNING: if you use the ‘w’ flag with fopen, as
% opposed to ‘r’, you will overwrite the file.

fid = fopen(filename, 'rt');
if fid <0
    wc = -1;
    return;
end

wc = 0;
oneline = fgets(fid);
while( ischar(oneline) )
    wc = wc + sum (isletter(oneline));
    oneline = fgets(fid);
end
