function p = prime_pairs(n)
% 5. Write a function called prime_pairs that returns the smallest prime
% number p smaller than 100,000 such that (p + n) is also prime, where n is
% a scalar integer and is the sole input argument to the function. If no
% such number exists, then the function returns -1. You may use the
% built-in functions, primes and isprime. Note that an efficient solution
% to this problem, such as the one the grader uses, takes a fraction of a
% second, but depending on how you do it, yours may be significantly
% slower.
p = -1;
for i = primes(100000)
    if isprime(i + n)
        p = i;
        return;
    end
end
