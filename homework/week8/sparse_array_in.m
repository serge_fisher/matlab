function A = sparse_array_in(filename)
% 2. Write a function called sparse_array_in that reads a two-dimensional
% array of doubles from a binary file whose name is provided by the single
% input argument of the function. The format of the file is specified in
% the previous problem. The function returns the two-dimensional array that
% it reads from the file as an output argument. Note that if you call
% sparse_array_out with an array called A and then call sparse_array_in
% using the same filename and save the output of the function in variable
% B, then A and B must be identical. If there is a problem opening the
% file, the function returns an empty array.
fid = fopen(filename, 'r');
if(fid <0)
    A = [];
    return;
end

params = fread(fid, 3, 'uint32');

A = zeros(params(1), params(2));

for i = 1:params(3)
    j = fread(fid, 2, 'uint32');
    A(j(1),j(2)) = fread(fid, 1, 'double');
end
