% 2. Write a function called checkerboard that takes as input two positive
% integer scalars, n and m, in that order. The function must create and
% return board, which is an n-by-m matrix. Every element of board is either
% 0 or 1. The first element, board(1,1) is 1. No direct neighbors in the
% matrix, vertically or horizontally, can be equal. That is, a 1 element
% cannot have 1 immediately preceding or following it in the same row or
% column.

function board = checkerboard(n, m)
preboard = zeros(n, m);
preboard(1:2:end,1:2:end) = 1;
preboard(2:2:end, 2:2:end) = 1;
board = preboard;