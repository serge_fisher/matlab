% 1. Write a function called quadrants that takes as its input argument a
% scalar integer named n. The function returns Q, a 2n-by-2n matrix. Q
% consists of four n-by-n submatrices. The elements of the submatrix in the
% top left corner are all 1s, the elements of the submatrix at the top
% right are 2s, the elements in the bottom left are 3s, and the elements in
% the bottom right are 4s.

function Q = quadrants(n)
topleft = ones(n);
topright = 2*ones(n);
bottomleft = 3*ones(n);
bottomright = 4*ones(n);
Q = [ topleft topright; bottomleft bottomright];
