# playground for MATLAB learning curve

just a repo for playing from scratch

* * *

## [v] DONE [8/8] Introduction to Programming with MATLAB (coursera course)

SCHEDULED: <2015-07-01 Wed>

(8 weeks course)

### [v]DONE Week1 (nothing serious, only blabla about installing and using)

#### Additional Material

* Download MATLAB
* [Lesson 1 Slides](intro_to_matlab_programming/Lesson_1.pdf)

----

### [v]DONE MATLAB Week2

SCHEDULED: <2015-07-08 ср>

- Note taken on [2015-07-09 чт 10:56] \\
      - matrices, creating (*rows* x *cols*)
      - colon operator (lowest priority, three operands like `10:-1:1`)
      - accessing parts of matrix (`matrix(1,:); matrix(:,1); m(:,1:4)`)
      - combining and transforming matrices
      - ariphmetics (array vs matrix operations; `a.*b` vs `a*b`)
      - division can be over (`/`) and under (`\`)
      - operator precedence (and associativity)


#### Additional Material

* [Lesson 2 Slides](intro_to_matlab_programming/Lesson_2.pdf)

#### [v]DONE Week 2 Quiz (10 questions)
Due: July 22, 2015 (11:59 pm in Hawaii)

----

### [v]DONE MATLAB Week3

SCHEDULED: <2015-07-15 ср>

- Note taken on [2015-07-14 вт 23:50] \\
      - edit [RET]
      - `v = a(:)` (when 'a' is matrix, this cause v to be a vector from
        'a')
      - see bitbucket/serge_fisher/matlab repo, I'll continue there
      - help exist
      - global v;
      - if you declare v as global inside some function, you need to
        declare it global in command window too, to see its content
      - scripts
          - made just like other matlab function files
          - `edit example_script`
          - always have 0 arity and have no any retval
          - scope of script is the scope of command window
      - `fprintf('End of lesson 3\n'); pause(3); quit;`

#### [v]DONE last video called 'Problem solving' (52 minutes, Carl!)

- every assignment is 10 problem
- every problem in its own dot-m file
- every problem set should be in its own directory
- it's a fucking simple instructions for dumb asses from north
  america. The lecturer seems to mimic cowboy's accent, he thinks it's
  funny and, well, he expects for his auditory to be alike.
- two problems were explained with most common errors like not passing
  the arguments or not returning a value from the function.
- What's this dot-p file extension is?

#### Additional Material

* [Lesson 3 Slides](intro_to_matlab_programming/Lesson_3.pdf)

#### [v]DONE MATLAB [Week3 Programming assignment](homework/week3)

* [FAQ](homework/FAQ.pdf) (Read this before you work on the homework!!!)

----

### [v] DONE MATLAB Week4
SCHEDULED: <2015-07-21 Tue>

1. Introduction to programmer's Toolbox  (7:06)
    - `sqrt(9);` and `sqrt([1 4; 9 16; 25 36])`
        + this is called polymorphism, when one function can take an
          argument of different types (sqrt takes an integer in first
          example and a matrix in the second)
        + not only a type of an argument, but also an arity
    - `sum([1 2;3 4])` returns `[4 6]`
    - `[a b] = max(1 2 -4 8])` returns a=8, b=4
        + a - maximum value, b - index of that value
    - `size([1 2;9 8;0 -1])` returns `[3 2]`, like dim() in R
    - functions can return a vector of two elements (like size) or just
      one and possibly another retval if asked to (like max)
    - for functions that returns value it's possible to ask them like
      we asked max function: `[rows cols] = size([1 2;3 4;5 0])`
2. Matrix Building   (15:11)
    - `zeroes(5,6); ones(4,2); 5*ones(4,2); zeroes(4);`
    - `zeroes(5,1); zeroes(1,5);` zeroes but diagonal
      `diag([1 2 3 4])`
    - `rand(3,4)` returns random matrix of 3x4 size, not the single
      integer in range 3..4 !
    - `rand(5)` returns a matrix of size 5x5 !!!
    - `fix( 1+ rand(5,4) *10)` to get a random integers in between 1
      and 10, but not just one - a whole matrix 5x4 of them!
    - `randi(10,5,4)` does exactly the same
    - 'randi' stands for 'random integers'
    - `randi([5 10], 2, 3)` returns a matrix 2x3 with integers 5..10
    - `randn(5)` for random value with normal distribution
    - `r = randn(1,100000); hist(r,100)` shows histogram for 10000
      random values with normal distribution
    - `rng(23)` is RNG init, like `random:seed(23)` or `set.seed(23)`
    - `rng('shuffle')` is like `random:seed(erlang:now())`
3. Input / Output  (20:47)
    - `x = input('gimme a number, buddy: ');`
        + you can type at the prompt `pi/2`
    - `fprintf('This is fprintf\n')`
    - `fprintf('%d items at %.2f each\nTotal = $%5.2f\n',
      n,price,total)`
    - `help fprintf` or `doc fprintf`
    - **NB**: to insert single quote instead of usual `\'` we should
      use `''` (single quote twice)
    - strange behaviour when number of variables doesnt the same as
      the number of escapes in format string. If lesser, the rest of
      the format string starting from unsatisfied escape will be
      dropped. If more, it starts to print like the format string
      glued into bigger one by concatenating two same format strings.
      Strange as hell.
    - that been done for vectors: `fprintf('%4.1f\n', [1 2 3 4 5 6]);`
4. Plotting  (17:47)
    - `a = (1:10).^2; plot(a)`
    - `figure(2)` to set figure 2 to plot to it
    - `a = (-10:10).^2; plot(a)`
    - previous figure on its x scale has indexes of vector a; for
      right scale we need to specify two vectors for x and y:
      `t = -10:10; b = t.^2; plot(t,b);`
    - two graphs simultaneously: `x1=0:0.1:2*pi; y1=sin(x1);
      x2=pi/2:0.1:3*pi; y2=cos(x2); plot(x1,y1,x2,y2)`
    - different colors: `plot(x1,y1,'r',x2,y2,'k:')`
    - lines and styles: `help plot`
    - `hold on` to add plotting to the same figure, not rewrite
    - `hold off` to replace plot in the figure with new one
    - `grid` to toggle grid in current figure
    - `title('a title for plot')`
    - `xlabel('xlab'); ylabel('y scale')` to add to the figure
    - `legend('sine', 'cosine')`
    - `axis([-2 12 -1.5 1.5])` to change scale
    - `close(2)` to close figure 2 `close all` to close all figures
5. Debugging  (22:17)
    - type of errors
        - syntax errors
        - semantic errors
    - they still uses debugger and break points. Click on grey area
      between line number and the very first symbol on that line to set
      a break point
    - when a break point reached the prompt will change to __K>>__
    - in workspace local variables will shown up
    - quit debugger with `dbquit` in `K>>` prompt

#### [v] DONE [Week4 Programming assignments](homework/week4) (5 problems)
DEADLINE: <2015-08-04 Tue>


#### Additional Material

* [Useful MATLAB built-in functions](intro_to_matlab_programming/Function_tables.pdf)
* [Lesson 4 Slides](intro_to_matlab_programming/Lesson_4.pdf)

----

### [v] DONE MATLAB Week5

1. Selection (11:53)
    - boring blabla 'bout control flow (with 'if' statement)
    - if [] elseif [] else [] end
    - condition without braces
    - 'fork in the road when you're driven' omg, what am i doing here?
2. If-Statements, continued (8:33)
    - `elseif` used as a switch in C
3. Relational and Logical Operators (34:51)
    - `~=` not equal, `==`, `>`, `<`, `<=`
    - logical values are in fact numerical (1 for true, 0 false)
    - precedence - first ariphmetic, then relational
        + `(16*64 > 1000) + 9` evals to 10
        + `16*64 > 1000 + 9` evals to 1
    - any non-zero number evals to true, including reals
    - integer 0, real 0.0, and empty string '', is false
    - matrix relations are one-to-one as an array arithmetics
        + `[4 -1 7 5 3] > [5 -9 6 5 -3]` evals to `[0 1 1 0 1]`
        + `[4 -1 7 5 3] <= 4` evals to `[1 1 0 0 1]`
        + `sum([14 9 3 14 8 3] == 14)` evals to 2 (similar to R)
    - logical operators `&&` (and) `||` (or) `~` (not) (tilde)
    - `b = 2; b*(0<b && b<10)` evals to 2
    - unary negation will work here: `~[1 pi 0 -2]` == `[0 0 1 0]`
    - binary logical fails with vectors:
        + `[1 -3 0 9 0] && [pi 0 0 2 3]` gives error
        + `[1 -3 0 9 0] & [pi 0 0 2 3]` evals to `[1 0 0 1 0]`
        + `2 & [0 1; 2 3]` evals to `[0 1; 1 1]`
        + `2 | [0 1; 2 3]` evals to `[1 1; 1 1]`
    - `help precedence`
4. Nested If-Statements (2:12)
    - too trivial
5. Variable Number of Function Arguments (6:40)
    - for writing polymorphic functions
    - we have two built-ins:
        + `nargin` returns the number of actual input arguments that
          the function was called with
        + `nargout` returns the number of output arguments that the
          function caller requested
    - [example of polymorphic function](intro_to_matlab_programming/Week5/multable.m)
6. Robustness (8:37)
    - `~isscalar(m) || m < 1 || m ~= fix(m)` check on input
    - comments between `function` keyword and the operators treated by
      matlab as a help message for this function. We can use help
      _function_name_ now to see this comment.
    - all-uppercase words in comments turns to bold in help
7. Persistent Variables (6:54)
    - there is third type of variables along with local and global
    - persistent variables is just like C static variables inside function
    - needs to be checked up at start --
    ```
        if isempty(var)
        var = 0
    ```
    - `clear` can apply to function with persistent variables to reset it
    - `if isempty(var), var=0; end`

#### [v] DONE [Week 5 Programming Assignments](homework/week5) (8 problems)
Deadline:  August 12, 2015  (11:59 pm in Hawaii)

#### Week 5 Additional Material

* [Lesson 5 Slides](intro_to_matlab_programming/Lesson_5.pdf)
* [Examples used in the lectures](intro_to_matlab_programming/Week5)

----

### [v] DONE MATLAB Week6

1. For-Loops  (38:50)
    - `for n = 1:5 ... end`
    - really boring explanation of 'for' loop, designed for cowboys
    - `list = rand(1,5); for x = list ... end`
        + loops through the elements of a list, index takes element
    - `for x = rand(1,5 ... end` == the same
    - the values assigned to the loop index do not have to be
        + integers,
        + regularly spaced, or
        + assigned in increasing order
    - in fact, they do not have to be scalars either:
        + the loop index will be assigned the columns of the array 
    - strange
      [fibonacchi function](intro_to_matlab_programming/Week6/fibo.m)
      which creates a vector in place
    - blabla about nesting for loops
2. While-Loops  (20:16)
    - we can use a `break` statement to quit for loop
    - `while total <= limit; n = n+1; total = total+n; end`
    - nice fun [approx_sqrt](intro_to_matlab_programming/Week6/approx_sqrt.m)
    - `15 + 3i` complex numbers
3. Break Statement  (29:31)
    - double percent signs means new section (and thus bold font)
    - we can run only particular section from a so-called script .m file
    - loop index after exiting from 'for' loop contains the last value
    - we can publish the script. Sections will look nice
    - `break` breaks only one level of loop (inner loop)
    - to break the outer loop he propose to set a flag before breaking
      from inner and check the flag on the outer loop.
    - alternative is to return immidiately with `return`
    - there is also a `continue` statement - skip the rest of the loop
      body and go straight to the next iteration
4. Logical Indexing  (37:29)
    - `w = v(v >= 0);` == the same as in R, logical indexing
    - boring explanation of logical indexing
    - logical vector looks just like integer vector with factor 0,1
    - `v(v<0)` shows the vector as a subset of original vector, **but**
    - `v(v<0)=0` return the vector of the size of original vector!
    - the general rule is the difference in lvalue and rvalue. When
      rvalue, it generates subset. When lvalue, it changes only
      selected
    - `v(v<0) = [100,200]` works perfect given that only two values
      will select in lvalue
    - `v(v<0) = v(v<10) + 100` is guaranteed to generate the same size
      vector on both lvalue and rvalue!
    - array with logical indexing is different
    - the array seems to be the same as in fortran
    - `A = [1 2 3; 4 5 6]; B = A (A>2);` produces **column** vector
      [4; 5; 3; 6]
    - this is because before applying logical vector MATLAB expands
      array into vector in column_major order (`A = A(:)`)
    - this is true for any dimension. Reminds me of fortran =)
    - but this is only true for **rvalue** position
    - when using logical indexing on **lvalue** the dimensions keeps
      the same intouch
    - `A(A<0) = 101:108` rvalue needs to be the same size as selected
      in lvalue, otherwise error would return
    - `A(A > 0.1) = sqrt(A(A > 0.1))` the sizes are guaranteed to be
      equal
    - `A = [89 82 11 53;33 5 59 42]; B = [34 44 52 64;62 73 58 99];`
    - `A((A>B)) = A(A>B) - B(A>B)` == comparing every element in array
      A to the corresponding element in B and if it's greater then
      replacing it with difference
5. Preallocation  (8:59)
    - `tic; sum(1:1e6); toc` to measure elapsed time
    - `zeros(5000,5000)` before loop makes memalloc and this is good
      cause MATLAB is dumb enought to realloc every iteration and this
      spends a lot of time
    - "_The varable 'A' appears to change size on every loop
      iteratino. Consider preallocating for speed._" -- this is the
      warning message of MATLAB in this case.

#### [v] DONE [Week 6 Programming Assignment](homework/week6) (10 problems)
Deadline:  August   19, 2015  (11:59 pm in Hawaii)

#### Week6 Additional Material

* [Lesson 6 Slides](intro_to_matlab_programming/Lesson_6.pdf)
* [Examples used in the lectures](intro_to_matlab_programming/Week6)

----

### [v] TODO MATLAB Week7

1. Introduction to Data Types  (20:27)
    - `class(x)` gives type of x
    - `whos` gives all the variables in current env
    - double, char, logical, single, integer (4 signed & 4 unsigned)
        + int8, int16, int32, in64, uint8, uint16, uint32, uint64
        + singe is 32bit float + Inf & NaN, double is single*2
    - `isa(x, 'double')` boolean is_double(x)
    - intmax, intmin
    - realmax, realmin
    - conversion by using desired type as a function name:
      `single(98.8)`, `uint8(23)`
    - `uint8(500)` -- if it more than ceiling it got maxvalue, `255` here
    - `uint8(-1)` -- gives 0 cause uint can't hold negatives
    - typing in matlab is not strong. After assigning an integer value
      to a variable, when assigning new value to the same varable the
      type of a varable will change silently.
2. Strings  (29:04)
    - string is a vector of char-s
    - ascii blabla
    - `fprintf('%s', char(ii));` == `fprintf('%s', ii);`
    - `strfind('MATLAB', 'LAB')` -> `4`
3. Structs  (14:51)
    - arrays are homogeneous, structs are heterogenous
        + fields, not elements
        + field names, not indices
        + fields in the same struct can have different types
        + field can contain other struct
    - `r.ssn = 1234568` creates struct `r` with field `ssn`
    - `r.name = 'homer simpson'` adds field to struct `r`
    - `r.address.street = '742 evergreen terrace'` new struct in new
      field
    - arrays can hold structs
    - `r(2).ssn = 9898` new struct item in array of `r`-s
    - `rmfield` doesn't change it's argument. Instead it _returns_
      changed argument as retval and you need to reassign it to the
      argument to make changes in place
4. Cells  (21:47)
    - pointers! MATLAB calls pointers a 'cell'
    - to access the data a cell points to, use `{ }` (AKA curly braces
      or curly brackets)
    - `c1 = {[1 2], [10,20]}` creates a cell `c1`
    - cells can not point to the same object
    - `c2 = c1` makes a copy of object pointed by c1 and creates a
      pointer to a new object
    - can be easily checked by changing values in data pointed by c1
      and stating that this does not affect data in c2

#### [v] TODO [Week 7 Programming Assignments](homework/week7) (8 problems)
Deadline:  August   26, 2015   (11:59 pm in Hawaii)

#### Additional Material

* [Lesson 7 Slides](intro_to_matlab_programming/Lesson_7.pdf)
* [Examples used in the lectures](intro_to_matlab_programming/Week7)

----

### [v] TODO MATLAB Week8

1. File Input/Output  (15:00)
    - blablabla about what files are, what directories are and how to
      change it. For total newbies /facepalm
    - `pwd`, `ls`, `contents = ls`, `whos contents`, `cd('subdir')`,
      `mkdir('subdir')`, `rmdir('subdir')`
    - `whos; save` saves workspace in matlab.mat; `load` loads
      workspace from matlab.mat
    - `save my_data_file var1 var2 var3` saves only named var1..var3
      into named file, my_data_file.mat
2. Excel Files  (9:12)
    - `xlsread` & `xlswrite`
    - `[num,txt,raw] = xlsread('Nashville_climate.xlsx;);`
    - `[~, txt] = xlsread('Nashville_climate.xlsx');` to skip first
      retvals we can use a tilda, `~`
    - `num = xlsread('Nashville_climate_data.xlsx', 1, 'D15')` to read
      only one single cell (1st sheet, D15 cell)
    - `nums = xlsread('Nashville_climate_data.xlsx', 1, 'D15:E17')` to
      read rectangular area from 1st sheet
3. Text Files  (12:17)
    - `fid = fopen(filename, permission)`, `fclose(fid)`
    - permission: 'rt', 'wr', 'at', etc `help fopen`
    - `oneline = fgets(fid);` to read one line from file
4. Binary Files  (25:23)
    - `fwrite(fid, A, 'double');` to write 'A' into a file
    - `A = fread(fid, inf, data_type);` to read array (number of items
      to read is 'inf', i.e. all the items); `data_type = 'double'`
    - explains why we need to write dimensions first and then the
      data, what 'format' is and why it needs to be
      documented. Simple.
    - farewell speech and wishes

#### [v] DONE [Final Programming Assignments](homework/week8) (10 problems)
Deadline:  September 2, 2015   (11:59 pm in Hawaii)

#### Additional Material

* [Lesson 8 Slides](intro_to_matlab_programming/Lesson_8.pdf)
* [Examples used in the lectures](intro_to_matlab_programming/Week8)

* * *